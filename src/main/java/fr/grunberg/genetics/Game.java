package fr.grunberg.genetics;

import java.util.Collection;

public interface Game {

	public void play();
	public Collection<? extends Object> getAssignedPlayers();
	public void addPlayer(AI newPlayer);
}
