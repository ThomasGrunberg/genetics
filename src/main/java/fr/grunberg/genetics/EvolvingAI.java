package fr.grunberg.genetics;

public interface EvolvingAI extends AI {
	public EvolvingAI cloneAndEvolve();
}
