package fr.grunberg.genetics;

import java.util.Collection;

public interface AI {
	public void initialize();
	public void calculateFitnessScore(Collection<Game> games);
	public int getFitnessScore();
	public String getOneLineDescriptionWithFitness();
}