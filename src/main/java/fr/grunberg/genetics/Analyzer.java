package fr.grunberg.genetics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SequencedCollection;
import java.util.function.Supplier;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;

public final class Analyzer {
	private final int numberOfCycles;
	private final int numberOfGamesPerCycle;
	private final int numberOfParallelAi;
	private final int numberOfAiToSelect;
	private final int numberOfPlayersPerGame;
	private final RandomGenerator randomGenerator;
	
	private final Logger logger;
	
	private final Supplier<AI> aiFactory;
	private final Supplier<Game> gameFactory;
	
	private SequencedCollection<AI> bestAIs;

	public Analyzer(int numberOfCycles, 
			int numberOfGamesPerCycle, 
			int numberOfParallelAi, 
			int numberOfAiToSelect, 
			int numberOfPlayersPerGame,
			RandomGenerator randomGenerator,
			Supplier<AI> aiFactory, 
			Supplier<Game> gameFactory, 
			Logger logger) {
		this.numberOfCycles = numberOfCycles;
		this.numberOfGamesPerCycle = numberOfGamesPerCycle;
		this.numberOfParallelAi = numberOfParallelAi;
		this.numberOfAiToSelect = numberOfAiToSelect;
		this.numberOfPlayersPerGame = numberOfPlayersPerGame;
		this.randomGenerator = randomGenerator;
		this.aiFactory = aiFactory;
		this.gameFactory = gameFactory;
		this.logger = logger;
	}
	
	public void analyze() {
		SequencedCollection<AI> ais = new ArrayList<>(numberOfParallelAi);
		for(int cycle = 1; cycle <= numberOfCycles; cycle++) {
			ais = fillIAs(ais);
			ais.stream().forEach(ai -> ai.initialize());
			Collection<Game> games = createGames(numberOfGamesPerCycle);
			assignPlayersToGames(ais, games);
			games.parallelStream().forEach(p -> p.play());
			ais = evaluateAIs(ais, games);
			logger.println("Cycle " + cycle);
			ais.reversed().stream().forEach(ia -> logger.println(ia.getOneLineDescriptionWithFitness()));
			logger.println("---");
			ais = removeBadAIs(ais);
			bestAIs = ais.stream().toList();
		}
	}

	SequencedCollection<AI> fillIAs(SequencedCollection<AI> ais) {
		if(ais.size() == numberOfAiToSelect) {
			Iterator<AI> aisToCloneAndEvolve = ais.stream().toList().iterator();
			while(aisToCloneAndEvolve.hasNext()) {
				AI ai = aisToCloneAndEvolve.next();
				if(ai instanceof EvolvingAI aiToCloneAndEvolve) {
					ais.add(aiToCloneAndEvolve.cloneAndEvolve());
				}
			}
		}
		while(ais.size() < this.numberOfParallelAi) {
			ais.add(aiFactory.get());
		}
		return ais;
	}

	Collection<Game> createGames(int numberOfGamesToCreate) {
		Collection<Game> games = new ArrayList<>(numberOfGamesToCreate);
		while(games.size() < numberOfGamesToCreate) {
			games.add(gameFactory.get());
		}
		return games;
	}

	void assignPlayersToGames(SequencedCollection<AI> ais, Collection<Game> games) {
		Map<AI, Integer> numberOfAssignedGamesPerAI = ais.stream()
				.collect(Collectors.toMap(ai -> ai, ai -> 0));
		for(Game game : games) {
			while(game.getAssignedPlayers().size() < numberOfPlayersPerGame) {
				AI newPlayer = getLeastPlayingAI(numberOfAssignedGamesPerAI, game.getAssignedPlayers().stream().filter(p -> p instanceof AI).map(ai -> (AI) ai).toList());
				game.addPlayer(newPlayer);
				numberOfAssignedGamesPerAI.compute(newPlayer, (ai, i) -> i + 1);
			}
		}
	}
	
	AI getLeastPlayingAI(Map<AI, Integer> numberOfAssignedGamesPerAI, Collection<AI> alreadyAssignedPlayers) {
		Map<AI, Integer> numberOfAssignedGamesPerAIWithoutAlreadyAssigned = 
				numberOfAssignedGamesPerAI.entrySet().stream()
				.filter(e -> !alreadyAssignedPlayers.contains(e.getKey()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		int minimumAssigned = numberOfAssignedGamesPerAIWithoutAlreadyAssigned.values().stream().mapToInt(Integer::intValue).min().getAsInt();
		List<AI> assignableAI = numberOfAssignedGamesPerAIWithoutAlreadyAssigned.entrySet().stream()
				.filter(e -> e.getValue() == minimumAssigned)
				.map(Entry::getKey)
				.collect(Collectors.toList());
		Collections.shuffle(assignableAI, randomGenerator);
		return assignableAI.getFirst();
	}

	SequencedCollection<AI> evaluateAIs(SequencedCollection<AI> ais, Collection<Game> games) {
		ais.parallelStream().forEach(ai -> ai.calculateFitnessScore(games));
		return ais.stream()
				.sorted((ai1, ai2) -> (ai2.getFitnessScore() - ai1.getFitnessScore()))
				.toList();
	}

	SequencedCollection<AI> removeBadAIs(SequencedCollection<AI> ais) {
		return ais.stream()
				.limit(numberOfAiToSelect)
				.collect(Collectors.toList());
	}

	public SequencedCollection<AI> getBestAIs() {
		return bestAIs.stream().toList();
	}

}
