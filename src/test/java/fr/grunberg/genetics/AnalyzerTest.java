package fr.grunberg.genetics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.SequencedCollection;
import java.util.random.RandomGenerator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnalyzerTest {
	private Sequencer sequencer;
	private Analyzer analyzer;
	
	@BeforeEach
	public void setUp() {
		sequencer = new Sequencer(1);
		analyzer = new Analyzer(3, 10, 20, 5, 2, 
				new FakeRandomGenerator(),
				() -> new SimpleAI(sequencer.next()),
				() -> new SimpleGame(),
				new NullLogger()
				);
	}
	
	@Test
	public void fillIAs() {
		SequencedCollection<AI> ais = analyzer.fillIAs(new ArrayList<>());
		assertEquals(20, ais.size());
	}
	@Test
	public void createGames() {
		Collection<Game> games = analyzer.createGames(10);
		assertEquals(10, games.size());
	}
	@Test
	public void assignPlayersToGames() {
		SequencedCollection<AI> ais = analyzer.fillIAs(new ArrayList<>());
		Collection<Game> games = analyzer.createGames(10);
		for(Game game : games) {
			assertEquals(0, game.getAssignedPlayers().size());
		}

		analyzer.assignPlayersToGames(ais, games);

		for(Game game : games) {
			assertEquals(2, game.getAssignedPlayers().size());
		}
		for(AI ai : ais) {
			assertTrue(games.stream()
					.anyMatch(g -> g.getAssignedPlayers().contains(ai))
					);
		}
		assertEquals(10, games.size());
	}
	@Test
	public void evaluateAIs() {
		SequencedCollection<AI> evaluatedAIs = analyzer.evaluateAIs(List.of(new SimpleAI(1), new SimpleAI(3), new SimpleAI(9)), Collections.emptyList());
		assertEquals(3, evaluatedAIs.size());
		assertEquals(9, evaluatedAIs.getFirst().getFitnessScore());
		assertEquals(1, evaluatedAIs.getLast().getFitnessScore());
	}
	@Test
	public void removeBadAIs() {
		SequencedCollection<AI> goodAIs = analyzer.removeBadAIs(analyzer.evaluateAIs(analyzer.fillIAs(new ArrayList<>()), Collections.emptyList()));
		assertEquals(5, goodAIs.size());
		assertEquals(20, goodAIs.getFirst().getFitnessScore());
		assertEquals(16, goodAIs.getLast().getFitnessScore());
	}
	@Test
	public void fullRun() {
		analyzer.analyze();
		assertEquals(5, analyzer.getBestAIs().size());
		assertEquals(50, analyzer.getBestAIs().getFirst().getFitnessScore());
		assertEquals(46, analyzer.getBestAIs().getLast().getFitnessScore());
	}
	@Test
	public void fullWithEvolveRun() {
		analyzer = new Analyzer(3, 10, 20, 5, 2, 
				new FakeRandomGenerator(),
				() -> new SimpleEvolvingAI(sequencer.next()),
				() -> new SimpleGame(),
				new NullLogger()
				);
		analyzer.analyze();
		assertEquals(5, analyzer.getBestAIs().size());
		assertEquals(40, analyzer.getBestAIs().getFirst().getFitnessScore());
		assertEquals(36, analyzer.getBestAIs().getLast().getFitnessScore());
	}
}

class FakeRandomGenerator implements RandomGenerator {
	@Override
	public long nextLong() {
		return 0;
	}
}

class NullLogger implements Logger {
	@Override
	public void println(String lineToPrint) {
	}
}

class SimpleAI implements AI {
	private final int strength;
	SimpleAI(int strength) {
		this.strength = strength;
	}
	@Override
	public void initialize() {}
	@Override
	public void calculateFitnessScore(Collection<Game> games) {}
	@Override
	public int getFitnessScore() {
		return strength;
	}
	@Override
	public String getOneLineDescriptionWithFitness() {
		return "SimpleAI(" + strength + ")";
	}
	@Override
	public String toString() {
		return getOneLineDescriptionWithFitness();
	}
}
class SimpleEvolvingAI extends SimpleAI implements EvolvingAI {
	SimpleEvolvingAI(int strength) {
		super(strength);
	}
	@Override
	public EvolvingAI cloneAndEvolve() {
		return new SimpleEvolvingAI(getFitnessScore()+1);
	}
}

class SimpleGame implements Game {
	private final Collection<AI> players = new ArrayList<>();

	@Override
	public void play() {}

	@Override
	public Collection<AI> getAssignedPlayers() {
		return players;
	}

	@Override
	public void addPlayer(AI newPlayer) {
		players.add(newPlayer);
	}
}

class Sequencer {
	private int initSequence;
	Sequencer(int initSequence) {
		this.initSequence = initSequence;
	}
	public int next() {
		return initSequence++;
	}
}