# Genetic algorithm Framework

## How to implement

- Add the package to your dependencies.
- Implement the Logger class for getting the result in text base.
- Implement the Game class over your game engine instance.
- Implement the AI class over your AI.
- Instantiate the Analyzer class, and call the analyze method. Either read the result in the Logger's output, or read the getBestAIs method's result.